const pool = require("../config");

class Colleges {
  static getAllCollege = async () => {
    const query = `
    SELECT * FROM college s ;
    `;

    const colleges = await pool.query(query);
    console.log(colleges);
    return colleges.rows;
  };

  static getCollege = async (id) => {
    const query = `SELECT * FROM college s WHERE s.id  = ${id};`;
    const college = await pool.query(query);
    console.log(college);
    return college.rows[0];
  };

  static createCollege = async (payload) => {
    const query = `INSERT INTO colleges
    (name, domisili, email, akreditasi)
    VALUES($1, $2, $3, $4)
    RETURNING *;
    `;
    const student = await pool.query(query, [
      payload.name,
      payload.domisili,
      payload.email,
      payload.akreditasi,
    ]);
    console.log(college);
    return college.rows[0];
  };

  static deleteCollege = async (id) => {
    const query = `
    DELETE FROM colleges
    WHERE id=${id}
    RETURNING *;
    `;
    const deleted = await pool.query(query);
    return deleted.rows[0];
  };

  static updateCollege = async (payload, id) => {
    let query = `
    UPDATE college SET
    `;
    const arr = Object.keys(payload);
    for (let i = 0; i < arr.length; i++) {
      if (i === arr.length - 1) {
        query += `${arr[i]}= '${payload[arr[i]]}'`;
      } else {
        query += `${arr[i]}= '${payload[arr[i]]}',`;
      }
    }
    query += `WHERE id=${id} RETURNING * ;`;
    console.log(query);
    const updated = await pool.query(query);
    return updated.rows[0];
  };
}

module.exports = Colleges;
