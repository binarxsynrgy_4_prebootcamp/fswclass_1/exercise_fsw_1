const studentsModel = require('./students');
const collegesModel = require('./colleges');

module.exports = {studentsModel, collegesModel};