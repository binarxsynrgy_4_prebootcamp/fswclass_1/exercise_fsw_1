const CollegeController = require('./CollegeController');
const StudentController = require('./StudentController');

module.exports = { CollegeController, StudentController};