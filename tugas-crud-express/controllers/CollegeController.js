const { collegeModel } = require("../models");

class CollegeController {
  static async getAllCollege(req, res) {
    const colleges = await collegeModel.getAllCollege();
    res.status(200).json({
      message: "Successfully fetched all colleges",
      colleges,
    });
  }

  static async getCollegeById(req, res) {
    const { id } = req.params;
    const college = await collegeModel.getCollege(id);
    res.status(200).json({
      message: "Successfully fetched college",
      college,
    });
  }

  static async createCollege(req, res) {
    const { name, domisili, email, akreditasi } = req.body;
    const payload = { name, domisili, email, akreditasi };
    const newCollege = await collegeModel.createCollege(payload);
    res.status(201).json({
      message: "Successfully created a new college",
      newCollege,
    });
  }

  static async updateCollege(req, res) {
    const { id } = req.params;
    let payload = {};
    for (let key in req.body) {
      payload[key] = req.body[key];
    }
    const updatedCollege = await collegeModel.updateCollege(payload, id);
    res.status(200).json({
      message: "Successfully updated college",
      updatedCollege,
    });
  }

  static async deleteCollege(req, res) {
    const { id } = req.params;
    await collegeModel.deleteCollege(id);
    res.status(200).json({
      message: "Successfully deleted a college",
    });
  }
}

module.exports = CollegeController;
