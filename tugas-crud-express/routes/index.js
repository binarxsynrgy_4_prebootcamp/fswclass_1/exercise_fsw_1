const CollegeRoute = require("../routes/CollegeRoute");
const StudentRoute = require("../routes/StudentRoute");
const route = require("express").Router();

route.get("/", (req, res) => {
  res.status(200).json({
    message: "Welcome to the CRUD API",
  });
});

route.use("/college", CollegeRoute);
route.use("/student", StudentRoute);

module.exports = route;
