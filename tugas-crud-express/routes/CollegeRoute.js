const { CollegeController } = require("../controllers");
const route = require("express").Router();

route.get("/", CollegeController.getAllCollege);
route.get("/:id", CollegeController.getCollegeById);
route.post("/", CollegeController.createCollege);
route.put("/:id", CollegeController.updateCollege);
route.delete("/:id", CollegeController.deleteCollege);

module.exports = route;
